<?php

/**
 * @file
 * Sort by the position of a nid in a views argument.
 */

/**
 * Sort nodes based on the position of the nid in an argument list.
 */
class views_handler_sort_argsort extends views_handler_sort {
  function query() {

    // Get the nid argument for the find_in_set() query statement.
    $ids = $this->view->argument['nid']->argument;

    // Handle alternative separator '+'
    $ids = str_replace('+', ',', $ids);

    // Escape the string, but don't need to check it more as the view won't work if the argument is badly formatted.
    // @todo, handle as a db_query argument.
    $ids = db_escape_string($ids);
    
    // Get the base field name as generically as we know how.
    $field = $this->query->base_table . '.' . $this->query->base_field;

    global $db_type;
    switch ($db_type) {
      case 'mysql':
      case 'mysqli':
        $formula = "find_in_set($field, \"$ids\")";
        break;
    }
    if (!empty($formula)) {
      $this->query->add_orderby(NULL, $formula, $this->options['order'], $this->field . '_' . $this->query->base_field);
    }
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
}
