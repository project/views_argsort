
Views Argument Sort
-------------------

This module effectively extends the functionality of a Views nid argument.
It provides a views Sort that means the View will be sorted in the same
order as the order of nids in the argument.

For example: http://example.com/my-view/9,1,4 
If have correctly configured a Views nid argument your query will be filtered
so that only nodes 9, 1 and 4 will display. By adding an Argument sort (ascending),
the nodes will be also ordered in this way.


Maintainers
-----------
sime (Simon Hobbs)

