<?php

/**
 * @file
 * Views field handler to dispay the index position of a nid in an argument.
 */

/**
 * For the returned nid, display its position in the nid arguments.
 * This is useful for debugging.
 */
class views_handler_field_argsort extends views_handler_field {

  function render($values) {
    if ($this->view->argument['nid']->argument) {
      // Get the nids argument and return the position of this nid in the arguments.
      $ids = explode(',', $this->view->argument['nid']->argument);
      $ids = str_replace('+', ',', $ids);
      $key = array_search($values->nid, $ids);
      return $key;
    }
    return '';
  }
  
}